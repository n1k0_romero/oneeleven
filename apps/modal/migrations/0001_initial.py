# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-08-18 16:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Proveedor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ruc', models.CharField(max_length=11, unique=True)),
                ('razon_social', models.CharField(max_length=150)),
                ('direccion', models.CharField(max_length=200)),
                ('telefono', models.CharField(max_length=15, null=True)),
                ('correo', models.EmailField(max_length=254, null=True)),
                ('estado', models.BooleanField(default=True)),
            ],
        ),
    ]
