from django.urls import reverse_lazy
from django.views.generic import (
    CreateView, 
    ListView, 
    DetailView,
    UpdateView,
)
from .forms import ProveedorForm
from .models import Proveedor


class ListadoProveedores(ListView):
    model = Proveedor
    template_name = 'modal/proveedores.html'
    context_object_name = 'proveedores'


class CrearProveedor(CreateView):
    template_name = 'modal/form.html'
    form_class = ProveedorForm
    success_url = reverse_lazy('modal:listado_proveedores')


class ModificarProveedor(UpdateView):
    model = Proveedor
    template_name = 'modal/form.html'
    form_class = ProveedorForm
    success_url = reverse_lazy('modal:listado_proveedores')


class DetalleProveedor(DetailView):
    model = Proveedor
    template_name = 'modal/detalle_proveedor.html'