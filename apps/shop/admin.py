from django.contrib import admin
from .models import (
	Category,
	Product,
)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'slug', )
    prepopulated_fileds = { 'slug': ('name', )}


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'slug', 'price', 'stock', 'available', 'created', 'updated', )
    list_filter = ('available', 'created', 'updated', )
    list_editable = ('price', 'stock', 'available', )
    prepopulated_fileds = { 'slug': ('name', )}
