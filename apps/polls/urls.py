from django.conf.urls import url
from .views import PollListView, PollDetailView

urlpatterns = [
    url(r'^list/$', PollListView.as_view(), name="list"),
    url(r'^detail/(?P<pk>[0-9a-f-]+)$', PollDetailView.as_view(), name="detail"),
]