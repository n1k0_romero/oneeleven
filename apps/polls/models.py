import uuid
from django.db import models


class Poll(models.Model):
	id = models.UUIDField(
		primary_key=True, 
		default=uuid.uuid4, 
		editable=False
	)
	name = models.CharField(
		max_length=30
	)

	def __str__(self):
		return '%s' % self.name
