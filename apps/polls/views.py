from django.views.generic import CreateView, ListView, DetailView
from django.views.generic.base import RedirectView
from django.core.urlresolvers import reverse
from .models import Poll


class PollCreateView(CreateView):
    model = Poll
    fields = ('name',)

    def get_success_url(self):
        return reverse('polls:list')


class PollListView(ListView):
    model = Poll
    fields = ('name',)
    template_name = 'polls/poll_list.html'


class PollDetailView(DetailView):
    model = Poll


class SearchRedirectView(RedirectView):
  url = 'https://google.com/?q=%(term)s'