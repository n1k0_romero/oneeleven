from django.shortcuts import render
from .models import OrderItem
from .forms import OrderCreateForm
from apps.cart.cart import Cart
from oneeleven.tasks import order_created, test_send_email


def order_create(request):
	cart = Cart(request)
	if request.method == 'POST':
		form = OrderCreateForm(request.POST)
		if form.is_valid():
			order = form.save()
			for item in cart:
				OrderItem.objects.create(
					order=order,
					product=item['product'],
					price=item['price'],
					quantity=item['quantity']
				)
			# clear the cart
			cart.clear()
			# launch asynchronous task
			test_send_email('n1k0_romero@hotmail.com')
			return render(
				request,
				'orders/order/created.html',
				{
					'cart': cart,
					'form': form
				}
			)
	else:
		form = OrderCreateForm()
	return render(
		request,
		'orders/order/create.html',
		{
			'cart': cart,
			'form': form
		}
	)
