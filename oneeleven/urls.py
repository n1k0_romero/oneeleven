"""oneeleven URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include, static
from django.contrib import admin
from django.conf import settings


urlpatterns = [
    url(r'^caronte/', admin.site.urls),
    url(r'^polls/', include('apps.polls.urls', namespace='polls')),
    url(r'^cart/', include('apps.cart.urls', namespace='cart')),
    url(r'^orders/', include('apps.orders.urls', namespace='orders')),
    url(r'^modal/', include('apps.modal.urls', namespace='modal')),
    url(r'^', include('apps.shop.urls', namespace='shop')),
]

# configuration for serve media files in local
if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)